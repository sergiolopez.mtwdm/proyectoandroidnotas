package com.example.notes.ui.notedetails

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.notes.R
import com.example.notes.core.Resource
import com.example.notes.data.local.AppDataBase
import com.example.notes.data.local.LocalDataSource
import com.example.notes.data.model.Note
import com.example.notes.data.remote.ApiClient
import com.example.notes.data.remote.NoteDataSource
import com.example.notes.databinding.FragmentNoteEditBinding
import com.example.notes.presentation.NoteViewModel
import com.example.notes.presentation.NoteViewModelFactory
import com.example.notes.repository.NoteRepositoryImp
import com.example.notes.ui.notes.adapters.NoteAdapter

class NoteEditFragment : Fragment(R.layout.fragment_note_edit) {
    private lateinit var binding: FragmentNoteEditBinding

    //    private val viewModel by viewModels<NoteViewModel> {
//        NoteViewModelFactory(NoteRepositoryImp(NoteDataSource(ApiClient.service)))
//    }
    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(
            NoteRepositoryImp(
                LocalDataSource(AppDataBase.getDataBase(this.requireContext()).noteDao()),
                NoteDataSource(ApiClient.service)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentNoteEditBinding.bind(view)

        binding.btnAddNote.setOnClickListener {
            var note = Note(
                0,
                binding.editTitle.text.toString(),
                binding.editContent.text.toString(),
                binding.editImageUrl.text.toString()
            )
            viewModel.saveNote(note).observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        findNavController().popBackStack()
                        /*
                        binding.progressbar.visibility = View.GONE
                        adapter = NoteAdapter(result.data.data) { note ->
                            //Toast.makeText(this.context, note.title, Toast.LENGTH_LONG)
                            onNoteClick(note)
                        }
                        binding.recyclerNotes.adapter = adapter
                        Log.d("LiveData", "${result.data.toString()}")
                         */
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Toast.makeText(
                            requireContext(),
                            "${result.exception.toString()}",
                            Toast.LENGTH_LONG
                        ).show()
                        /*
                        binding.progressbar.visibility = View.GONE
                        Log.d("LiveData", "${result.exception.toString()}")
                         */
                    }
                }
            })
        }

    }
}