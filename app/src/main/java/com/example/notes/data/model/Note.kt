package com.example.notes.data.model

data class Note(
    val id: Int = 0,
    val title: String = "",
    val content: String = "",
    val image: String = ""
)