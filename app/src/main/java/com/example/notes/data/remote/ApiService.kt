package com.example.notes.data.remote

import com.example.notes.data.model.Note
import com.example.notes.data.model.NoteList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    //https://testapi.io/api/sergiolopezmtwdm/resource/notes
    @GET("notes")
    suspend fun getNotes(): NoteList

    @POST("notes")
    suspend fun saveNote(@Body note: Note?) : Note?
}